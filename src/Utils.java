import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    private ArrayList<Bucket> listOfBuckets;
    private Counter counter;
    private int N;

    public Utils() {
        this.listOfBuckets = new ArrayList<>();
        this.counter = new Counter();
    }

    public void addBit(String bit) {
        if (bit.equals("1")) {
            int bucketSize = 0;
            counter.addOne();
            listOfBuckets.add(new Bucket(counter.getTimeStamp(), counter.getTimeStamp(), bucketSize));
            removeBucketIfEndPriorToN();

            while (true) {
                if (isThereThreeBucketsOfSize(bucketSize)) {
                    mergeTwoOldestBucketsOfSize(bucketSize);
                    bucketSize++;
                } else {
                    break;
                }
            }
        } else {
            counter.addOne();
            removeBucketIfEndPriorToN();
        }


    }

    private boolean isThereThreeBucketsOfSize(int bucketSize) {
        Map<Integer, Integer> map = new HashMap<>();

        for (Bucket bucket : this.listOfBuckets) {
            Integer key   = bucket.getBucketSize();
            Integer value = map.get(key);
            if (map.containsKey(key)) {
                map.replace(key, value + 1);
            } else {
                map.put(key, 1);
            }
            if (map.containsKey(bucketSize) && map.get(bucketSize) > 2) {
                return true;
            }
        }

        return false;

    }

    private void mergeTwoOldestBucketsOfSize(int bucketSize) {
        ArrayList<Bucket> threeBucketList = getThreeBucketsOfSize(bucketSize);
        int biggestBeginTimeStamp         = getBiggestBeginTimeStamp(threeBucketList);
        int newBeginTimeStamp             = getNewBeginTimeStamp(threeBucketList, biggestBeginTimeStamp);
        int newEndTimeStamp               = getNewEndTimeStamp(threeBucketList, biggestBeginTimeStamp);
        removeTwoOldestBucketsOfSize(bucketSize, biggestBeginTimeStamp); //send biggest so that I know which not to touch
        this.listOfBuckets.add(new Bucket(newBeginTimeStamp, newEndTimeStamp, bucketSize+1));

    }

    private ArrayList<Bucket> getThreeBucketsOfSize(int bucketSize) {
        ArrayList<Bucket> threeBucketList = new ArrayList<>();
        for (Bucket bucket : this.listOfBuckets) {
            if (bucket.getBucketSize() == bucketSize)
                threeBucketList.add(bucket);
        }


        return threeBucketList;
    }

    private int getBiggestBeginTimeStamp(ArrayList<Bucket> threeBucketList) {
        int biggestBeginTimeStamp = threeBucketList.get(0).getBeginTimeStamp();
        for (Bucket bucket : threeBucketList)
            if (bucket.getBeginTimeStamp() > biggestBeginTimeStamp)
                biggestBeginTimeStamp = bucket.getBeginTimeStamp();

        return biggestBeginTimeStamp;
    }

    private int getNewBeginTimeStamp(ArrayList<Bucket> threeBucketList, int biggestBeginTimeStamp) {
        int newBeginTimeStamp = Integer.MAX_VALUE; //TODO pazit na overflow
        for (Bucket bucket : threeBucketList){
            if (bucket.getBeginTimeStamp() != biggestBeginTimeStamp && bucket.getBeginTimeStamp() < newBeginTimeStamp)
                newBeginTimeStamp = bucket.getBeginTimeStamp();
        }

        return newBeginTimeStamp;
    }

    private int getNewEndTimeStamp(ArrayList<Bucket> threeBucketList, int biggestBeginTimeStamp) {
        int newEndTimeStamp = Integer.MIN_VALUE; //TODO mogu probat sa zastavicama, al je onda kod ruzniji
        for (Bucket bucket : threeBucketList){
            if (bucket.getBeginTimeStamp() != biggestBeginTimeStamp && bucket.getEndTimeStamp() > newEndTimeStamp)
                newEndTimeStamp = bucket.getEndTimeStamp();
        }

        return newEndTimeStamp;
    }

    private void removeTwoOldestBucketsOfSize(int bucketSize, int biggestBeginTimeStamp) {
        for (Bucket bucket : this.listOfBuckets) {
            if (bucket.getBucketSize() == bucketSize && bucket.getBeginTimeStamp() != biggestBeginTimeStamp) {
                this.listOfBuckets.remove(bucket); //TODO provjerit da se index trenutne ne poremeti, pa onda vratit u jednu for petlju
                break;
            }
        }
        for (Bucket bucket : this.listOfBuckets) {
            if (bucket.getBucketSize() == bucketSize && bucket.getBeginTimeStamp() != biggestBeginTimeStamp) {
                this.listOfBuckets.remove(bucket);
                break;
            }
        }
    }

    private void removeBucketIfEndPriorToN() {
        for (Bucket bucket : this.listOfBuckets) {
            if (bucket.getEndTimeStamp() < this.counter.getTimeStamp()+1 - this.N) {
                this.listOfBuckets.remove(bucket);
                break;
            }
        }
    }

    public ArrayList<Bucket> getListOfBuckets() {
        return this.listOfBuckets;
    }

    public Counter getCounter() {
        return counter;
    }

    public void setN(int N) {
        this.N = N;
    }
}
