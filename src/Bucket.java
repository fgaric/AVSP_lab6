public class Bucket {

    private int beginTimeStamp;
    private int endTimeStamp;
    private int bucketSize;

    public Bucket(int beginTimeStamp, int endTimeStamp, int bucketSize) {
        this.beginTimeStamp = beginTimeStamp;
        this.endTimeStamp   = endTimeStamp;
        this.bucketSize     = bucketSize;
    }

    public int getBucketSize() {
        return bucketSize;
    }

    public int getBeginTimeStamp() {
        return beginTimeStamp;
    }

    public int getEndTimeStamp() {
        return endTimeStamp;
    }

    @Override
    public String toString() {
        return "(" + this.beginTimeStamp + "," + this.endTimeStamp + "=" + Math.pow(2, this.bucketSize) + ")";
    }

}
