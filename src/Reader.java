import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Reader {
    private Utils utils;

    public Reader() {
        utils = new Utils();
    }


    public void begin() throws Exception {
        BufferedReader bReader = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\testovi\\t2.in")));
        //BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.valueOf(bReader.readLine());
        utils.setN(N);
        String line;

        try {
            while (!(line = bReader.readLine()).isEmpty()) {
                if (line.startsWith("0") || line.startsWith("1")) {
                    streamOfBits(line);
                } else {
                    System.out.println(inquiry(line));

                }
            }
        } catch (Exception ex) {
        }

    }

    private void streamOfBits(String line) {
        ArrayList<String> listOfBits = convertLineOfBitsToArray(line);
        for (String bit : listOfBits) {
            utils.addBit(bit);
        }
    }

    private ArrayList<String> convertLineOfBitsToArray(String line) {
        ArrayList<String> listOfBits = new ArrayList<>(); //TODO probat sa one-linerom mozda
        for (char c : line.toCharArray()) {
            listOfBits.add(String.valueOf(c));
        }
        return listOfBits;

    }

/*   private int inquiry(String line) {
        int numberOfOnes = 0;
        String k = line.split(" ")[1];
        for (Bucket bucket : this.utils.getListOfBuckets()) {
            int countStamp = this.utils.getCounter().getTimeStamp();
            if ((bucket.getBeginTimeStamp()) > (countStamp+1 - Integer.valueOf(k))) {
                numberOfOnes += Math.pow(2, bucket.getBucketSize());
            } else if (((bucket.getBeginTimeStamp()) <= (countStamp+1 - Integer.valueOf(k))) && ((bucket.getEndTimeStamp()) >= (countStamp+1 - Integer.valueOf(k)))) {
                numberOfOnes += Math.floor(Math.pow(2, bucket.getBucketSize()) / 2);
            }
        }
        return numberOfOnes;

    }*/

    private int inquiry(String line) {
        this.utils.getListOfBuckets().sort(new SortByEndTime());
        int numberOfOnes = 0;
        int k = Integer.valueOf(line.split(" ")[1]);
        for (int i=0; i < this.utils.getListOfBuckets().size(); i++) {
            if (this.utils.getListOfBuckets().get(i).getEndTimeStamp() >= this.utils.getCounter().getTimeStamp()+1 - k) { //ulazi
                if (this.utils.getListOfBuckets().size() <= i+1 || this.utils.getListOfBuckets().get(i+1).getEndTimeStamp() < this.utils.getCounter().getTimeStamp()+1 - k) {//ali onaj poslije njega, ne
                    numberOfOnes += Math.floor(Math.pow(2, this.utils.getListOfBuckets().get(i).getBucketSize()) / 2);
                    break;
                } else {
                    numberOfOnes += Math.pow(2, this.utils.getListOfBuckets().get(i).getBucketSize());
                }
            }
        }
        return numberOfOnes;
    }

}
