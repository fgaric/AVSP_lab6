public class Counter {

    private int timeStamp;

    public Counter() {
        this.timeStamp = -1;
    }

    public void addOne() {
        this.timeStamp ++;
    }

    public int getTimeStamp() {
        return this.timeStamp;
    }
}
