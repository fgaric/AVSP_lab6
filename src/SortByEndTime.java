import java.util.Comparator;

public class SortByEndTime implements Comparator<Bucket> {
    @Override
    public int compare(Bucket o1, Bucket o2) {
        return Integer.compare(o2.getEndTimeStamp(), o1.getEndTimeStamp());
    }
}
